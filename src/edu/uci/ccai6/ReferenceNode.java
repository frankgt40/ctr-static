package edu.uci.ccai6;

import soot.SootField;
import edu.osu.cse.pa.spg.AbstractAllocNode;

public class ReferenceNode {
	public AbstractAllocNode from;
	public SootField ptFld;
	public ReferenceNode(AbstractAllocNode base, SootField ptFld/*, AbstractAllocNode dst*/) {
		super();
		this.from = base;
		this.ptFld = ptFld;
	}
	

	public ReferenceNode() {
		
	}
	
	public boolean equals(ReferenceNode n2) {
		return (this.from == n2.from && this.ptFld == n2.ptFld /*&& this.to == n2.to*/);
	}
}
