package edu.uci.ccai6;

import java.util.List;

import edu.uci.ccai6.util.Helper;
import soot.PatchingChain;
import soot.SootClass;
import soot.SootMethod;
import soot.Unit;
import soot.ValueBox;
import soot.jimple.InvokeStmt;
import soot.jimple.Stmt;
import soot.jimple.internal.AbstractInvokeExpr;

public class Site {
	private SootMethod mtd;
	private int lineNum = 0;
	
	public Site(SootMethod mtd, int lineNum) {
		this.mtd = mtd;
		this.lineNum = lineNum;
	}


	public SootMethod getMtd() {
		return mtd;
	}

	public int getLineNum() {
		return lineNum;
	}
	
	public void printSite() {
		System.out.println(mtd.getSignature() + ": " + lineNum);
	}
	public Stmt getStmt(String calleeName) {
		PatchingChain<Unit> units = mtd.getActiveBody().getUnits();
		for (Unit u : units) {
			if (u.toString().contains("main"))
				System.out.println("");
			int l = Helper.getLineNumFromTag(u);
			if ( lineNum == l && u.toString().contains(calleeName)
				|| l == -1 && u.toString().contains(calleeName)	) { //frankgt40: if the taglist of this unit is empty we should avoid comparing the linenumber
				List<ValueBox> boxes = u.getUseBoxes();
				for (ValueBox box : boxes) {
					if (box.getValue() instanceof AbstractInvokeExpr) {
						return (Stmt)u;
					}
				}
			}
		}
		return null;
	}
}
