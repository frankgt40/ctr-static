package edu.uci.ccai6;

import java.util.List;

import edu.osu.cse.pa.spg.AbstractSPGEdge;
import soot.jimple.toolkits.callgraph.Edge;

public class CtxStack {
	List<AbstractSPGEdge>	ctxStack = null;

	public CtxStack(List<AbstractSPGEdge> ctxStack) {
		super();
		this.ctxStack = ctxStack;
	}

	public List<AbstractSPGEdge> getCtxStack() {
		return ctxStack;
	}

	public void setCtxStack(List<AbstractSPGEdge> ctxStack) {
		this.ctxStack = ctxStack;
	}
	
	
}
