package edu.uci.ccai6.graphviz.util;

public enum OutputType {
	GIF ("-Tgif", ".gif"),
	PS("-Tps", ".ps"),
	PNG("-Tpng", ".png");
	
	private final String param;
	private final String post;

	OutputType(String param, String post) {
		this.param = param;
		this.post = post;
	}
	public String param() {
		return param;
	}
	public String post() {
		return post;
	}
}
