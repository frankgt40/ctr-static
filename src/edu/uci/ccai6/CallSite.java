package edu.uci.ccai6;

import edu.uci.ccai6.util.Helper;
import soot.PatchingChain;
import soot.SootMethod;
import soot.Unit;
import soot.jimple.Stmt;

public class CallSite {
	private Site site = null;
	private Stmt callSite = null;
	private SootMethod mtd = null;

	public CallSite(Site site) {
		this.site = site;
		this.mtd = site.getMtd();
		int lineNum = site.getLineNum();
		PatchingChain<Unit> units = mtd.retrieveActiveBody().getUnits();
		for (Unit u : units) {
			Stmt stmt = (Stmt) u;
			int l = Helper.getLineNumFromTag(u);
			if (l == lineNum && stmt.containsInvokeExpr()) {
				// if (stmt.toString().contains(nextMtdName)) {
				callSite = stmt;
				// }
			}
		}
	}
	
	
	public Site getSite() {
		return site;
	}


	public Stmt getCallSite() {
		return callSite;
	}


	public SootMethod getMtd() {
		return mtd;
	}


	public void print() {
		System.out.println("Method: " + mtd + "; Call Site: "+callSite);
	}
}
