package edu.uci.ccai6.dumps;

public class HeapDump {
	public int no;
	public HeapObj from;
	public HeapObj to;
	public HeapDump(int no, HeapObj from, HeapObj to) {
		super();
		this.no = no;
		this.from = from;
		this.to = to;
	}
	
}
