package edu.uci.ccai6.dumps;

/**
 * @author frankgt40
 *
 */
public class AllocSite {
//	String className;
//	String funcDescrpt;
//	String returnType;
//	int lineNum;
//	public AllocSite(String className, String funcDescrpt, String returnType,
//			int lineNum) {
//		super();
//		this.className = className;
//		this.funcDescrpt = funcDescrpt;
//		this.returnType = returnType;
//		this.lineNum = lineNum;
//	}
	String line; // temperory
	public AllocSite(String line) {
		if (line == null) return;
		if (line.contains("Wrong site! Should not be used!")) {
			this.line = null;
		} else {
			line = line.replace("[invisible method]", "");
			line = line.replace("\n", "");
			this.line = line;
		}
	}
	public boolean isEmpty() {
		return (line == null) || line.trim().isEmpty();
	}
}
