package edu.uci.ccai6.dumps;

import java.util.List;

import soot.PatchingChain;
import soot.SootMethod;
import soot.Unit;
import soot.jimple.Stmt;
import soot.tagkit.LineNumberTag;
import soot.tagkit.Tag;

import java.util.ArrayList;
import java.util.Iterator;

import edu.uci.ccai6.Site;
import edu.uci.ccai6.util.Helper;
import edu.uci.ccai6.util.Helper2;

public class Leak {
	List<Site> callingContext;
	Site allocSite;
	String no;
	String type;
	String leakName;
	String allocName;
	String opType;
	public Site getLastUseSite() {
		return callingContext.get(0);
	}
	public List<Site> getCallingContext() {
		return callingContext;
	}

	public Site getAllocSite() {
		return allocSite;
	}

	public String getType() {
		return type;
	}
	public String getLeakObjName() {
		return this.leakName;
	}
	public String getAllocObjName() {
		return this.allocName;
	}
	public String getOpType() {
		return opType;
	}
	public void setOpType(String opType) {
		this.opType = opType;
	}
	public String getNo() {
		return no;
	}
	public void setNo(String no) {
		this.no = no;
	}
	public Leak(String no, String lastUseSites, String allocSiteStr, String type, String  leakName, String allocName, String opType) {
		this.no = no;
		this.type = Helper.convertTypeSig(type);
		this.allocSite = Helper.lineToSite(allocSiteStr);
		this.callingContext = new ArrayList<Site>();
		this.leakName = leakName;
		this.allocName = allocName;
		this.opType = opType;
		String[] lines = lastUseSites.split("\n");
		for (String line : lines) {
			if (!line.trim().isEmpty()) {
				Site tmp = Helper.lineToSite(line);
				if (tmp != null)
					this.callingContext.add(tmp);
			}
		}
	}
	public Leak(String callingContextStr, String allocSiteStr, String type) {
		this.type = Helper2.realType(type);
		allocSite = Helper2.lineToSite(allocSiteStr);
		callingContext = new ArrayList<Site>();
		String[] lines = callingContextStr.split("\n");
		for (int i = 0; i < lines.length; i++) {
			if (!lines[i].trim().isEmpty()) {
//				System.out.println("Line: " + lines[i]);
				Site tmp = Helper2.lineToSite(lines[i]);
				if (tmp != null)
					callingContext.add(tmp);
//				System.out.print("tmp: "); tmp.printSite();
			}
		}
//		System.out.println("Success!");
//		System.exit(-1);
	}
	
	public void print() {
		System.out.println("Alloc Site: ");
		allocSite.printSite();
		System.out.println("Calling context:");
		for (Iterator<Site> i = callingContext.iterator(); i.hasNext();) {
			Site site = i.next();
			site.printSite();
		}
	}
}
