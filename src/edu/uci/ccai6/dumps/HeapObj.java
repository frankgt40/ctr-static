package edu.uci.ccai6.dumps;

public class HeapObj {
	public String type;
	public AllocSite allocSite;
	public long addr;
	public HeapObj(String type, AllocSite allocSite, long addr) {
		super();
		this.type = type;
		this.allocSite = allocSite;
		this.addr = addr;
	}
	public String toString() {
		String rsl = "";
		if (!hasAllocSite()) return rsl;
		rsl += "[Type: " + type + " @ Alloc: " + allocSite.line+ "]";
		return rsl;
	}
	public boolean equals(HeapObj o2) {
		if (!hasAllocSite()) return false; 
		return this.allocSite.line.equals(o2.allocSite.line);
	}
	public boolean hasAllocSite() {
		return !allocSite.isEmpty();
	}
}
