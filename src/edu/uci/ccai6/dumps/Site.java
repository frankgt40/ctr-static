package edu.uci.ccai6.dumps;

import soot.SootClass;
import soot.SootMethod;

public class Site {
	private SootMethod mtd;
	private int lineNum = 0;
	
	public Site(SootMethod mtd, int lineNum) {
		this.mtd = mtd;
		this.lineNum = lineNum;
	}


	public SootMethod getMtd() {
		return mtd;
	}

	public int getLineNum() {
		return lineNum;
	}
	
	public void printSite() {
		System.out.println(mtd.getSignature() + ": " + lineNum);
	}
}
