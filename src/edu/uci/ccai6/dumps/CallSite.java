package edu.uci.ccai6.dumps;

import java.util.Iterator;

import edu.uci.ccai6.Site;
import soot.Local;
import soot.PatchingChain;
import soot.SootMethod;
import soot.Unit;
import soot.jimple.NewExpr;
import soot.jimple.Stmt;
import soot.tagkit.LineNumberTag;
import soot.tagkit.Tag;

public class CallSite {
	private Site site = null;
	private Stmt callSite = null;
	private SootMethod mtd = null;

	public CallSite(Site site) {
		this.site = site;
		this.mtd = site.getMtd();
		int lineNum = site.getLineNum();
		PatchingChain<Unit> units = mtd.retrieveActiveBody().getUnits();
		for (Unit u : units) {
			Stmt stmt = (Stmt) u;
			Integer l = new Integer(0);
			for (Iterator i = u.getTags().iterator(); i.hasNext();) {
				Tag tag = (Tag) i.next();
				if (tag instanceof LineNumberTag) {
					byte[] value = tag.getValue();
					int lineNumber = ((value[0] & 0xff) << 8) | (value[1] & 0xff);
					if (lineNumber != l.intValue())
						l = new Integer(lineNumber);
				}
			}
			if (l == lineNum) {
				if (stmt.containsInvokeExpr()) {
					// if (stmt.toString().contains(nextMtdName)) {
					callSite = stmt;
					// }
				}
			}
		}
	}
	
	
	public Site getSite() {
		return site;
	}


	public Stmt getCallSite() {
		return callSite;
	}


	public SootMethod getMtd() {
		return mtd;
	}


	public void print() {
		System.out.println("Method: " + mtd + "; Call Site: "+callSite);
	}
}
