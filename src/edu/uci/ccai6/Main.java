package edu.uci.ccai6;

import iohoister.analysis.MayAliasAnalysis;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import edu.osu.cse.pa.spg.AbstractAllocNode;
import edu.osu.cse.pa.spg.AbstractNode;
import edu.osu.cse.pa.spg.AbstractSPGEdge;
import edu.osu.cse.pa.spg.AllocNode;
import edu.osu.cse.pa.spg.ClassConstNode;
import edu.osu.cse.pa.spg.EntryEdge;
import edu.osu.cse.pa.spg.ExitEdge;
import edu.osu.cse.pa.spg.FieldPTEdge;
import edu.osu.cse.pa.spg.FieldVarNode;
import edu.osu.cse.pa.spg.NodeFactory;
import edu.osu.cse.pa.spg.StringConstNode;
import edu.osu.cse.pa.spg.SymbolicPointerGraph;
import edu.osu.cse.pa.spg.VarNode;
import edu.osu.cse.pa.spg.symbolic.SymbolicGlobalObject;
import edu.osu.cse.pa.spg.symbolic.SymbolicObject;
import edu.uci.ccai6.graphviz.util.OutputType;
import edu.uci.ccai6.util.Drawer;
import alias.AliasCache;
import alias.CallSite;
import alias.FldPair;
import alias.Pair;
import alias.TraverseTuple;
import alias.Util;
import alias.WildcardEdge;
import soot.Local;
import soot.MethodOrMethodContext;
import soot.PackManager;
import soot.PatchingChain;
import soot.Scene;
import soot.SceneTransformer;
import soot.SootClass;
import soot.SootField;
import soot.SootMethod;
import soot.Transform;
import soot.Unit;
import soot.Value;
import soot.jimple.AssignStmt;
import soot.jimple.IdentityStmt;
import soot.jimple.InvokeStmt;
import soot.jimple.NewExpr;
import soot.jimple.Stmt;
import soot.jimple.toolkits.callgraph.Edge;
import soot.jimple.toolkits.callgraph.ReachableMethods;
import soot.options.Options;
import soot.tagkit.LineNumberTag;
import soot.tagkit.Tag;
import soot.util.Chain;
import soot.util.queue.QueueReader;

public class Main extends SceneTransformer {
	private static Main mayAliaser;
	private static final int MAX_ITERATION = 100000;
	private static Translator tr = null;
	
	
	/// For debug
	private static Local tmp28;
	private static Local tmp29;
	private static SootMethod init;
	private static SootMethod add;
	
	private static String BM = ""; // Specify the benchmark name to analyze
	public static int refLen = -1; // the threshold of desired references length
	public static int N_BUDGET = 40000; // the node budget
	public void run() {
		tr = Translator.v();
		
		tr.buildLeakList("."+File.separator+"input"+File.separator+"fromDynamic"+File.separator+BM+".txt");
		tr.translate();
		AECUtil.out.close();
	}
	
	public static void printSiStackSet(Set<SiStack> siStackSet) {
		System.out.println("Printing the SiStack: ");
		String prompt = "";
		for (SiStack siStack : siStackSet) {
			prompt += printSiStack(siStack);
		}
	}
	public static String printSiStack(SiStack siStack) {
		return siStack.toString();
	}
	
	@Override
	protected void internalTransform(String arg0, Map arg1) {
		long start = System.currentTimeMillis();
		Translator.maa = edu.osu.cse.pa.Main.v();
		Translator.maa.buildSPG();
		run();
		long end = System.currentTimeMillis();
		long delta = end - start;
//		System.out.println("Time: " + delta);
	}

	private Main() {

	}

	public static Main v() {
		if (mayAliaser == null) {
			mayAliaser = new Main();
		}
		return mayAliaser;
	}

	public static void main(String[] args) {
		LinkedList<String> argList = new LinkedList<String>(Arrays.asList(args));
		for (int i = 0; i < argList.size(); i++) {
			if (argList.get(i).matches("dacapo\\..*\\.Main")) {
				BM = argList.get(i).replaceAll("dacapo\\.", "").replaceAll("\\.Main", "");
				break;
			}
		}
		for (int i = 0; i < argList.size(); i++) {
			if (argList.get(i).equals("-refLen")) {
				refLen = Integer.parseInt(argList.get(i+1));
				argList.remove(i);
				argList.remove(i);
				break;
			}
		}
		for (int i = 0; i < argList.size(); i++) {
			if (argList.get(i).equals("-budget")) {
				N_BUDGET = Integer.parseInt(argList.get(i+1));
				argList.remove(i);
				argList.remove(i);
				break;
			}
		}
		
		Main mayAliaser = Main.v();

		String phaseName = "wjtp.slicer";
		Transform t = new Transform(phaseName, mayAliaser);
		PackManager.v().getPack("wjtp").add(t);

		System.setProperty("MayAlias", "spa");
		boolean isPaddle = Util.MAY_ALIAS.equals("paddle");
		
		
		String JRE = "/home/chengcai/Softwares/jdk1.7.0_80/jre/";
		String sootClassPath = JRE+"/lib/rt.jar:"+JRE+"/lib/jce.jar:"+JRE+"/lib/jsse.jar";//:${B}-deps.jar:${TEMP}"
		String classFolder = "./input/srcForStatic/";
		String mainClass = null;
		if (BM.equals("test")) {
			mainClass = "Test";
			classFolder = classFolder + "test/";
		} else {	
			mainClass = "dacapo."+BM+".Main";
			classFolder = classFolder + "/dacapoBench/";
		}
		classFolder += ":"+sootClassPath;


		Options.v().set_whole_program(true);
		Options.v().setPhaseOption("cg", "enabled:true");
		Options.v().setPhaseOption("wjpp", "enabled:true");
		Options.v().setPhaseOption("wjap", "enabled:true");

		Options.v().setPhaseOption("jap", "enabled:true");
		Options.v().setPhaseOption("jb", "use-original-names:false");

		Options.v().set_keep_line_number(true);
//		Options.v().set_output_format(Options.output_format_jimple);

		// Important: you need to add rt.jar of jre to your CLASSPATH
		Options.v().set_prepend_classpath(true);
		
		Options.v().set_full_resolver(true);

		/**
		 * args should be in the following format:
		 * "-cp path_of_classes_analyzed class_names" e.g., -cp
		 * E:\Workspace\ws_program\taintanalysis\bin\ InterTest HelloWorld
		 */
//		String[] args2 = { "-cp", classFolder, "-main-class", mainClass,// "-soot-class-path", sootClassPath,
//				mainClass };-cp ${JRE_HOME}/lib/rt.jar:${JRE_HOME}/lib/jce.jar:${JRE_HOME}/lib/jsse.jar:./input/srcForStatic/dacapoBench  -main-class dacapo.${BM}.Main
//		String[] args2 = { "-cp", "./input/fromStatic/test", "-main-class", "Test", "Test"};//,// "-soot-class-path", sootClassPath,

		BM = AECUtil.benchName;
		
		System.out.println("[CR(N) PARAMETERS] Benchmark: " + BM + ", N: "+ refLen + ", Node budget: " + N_BUDGET);
		args = AECUtil.cliCmd.split(" ");
//		if (BM.equals("test")) {
//			args = args2;
//		} else {
//		    args=argList.toArray(new String[argList.size()]);
//		}
		for (String a : args) {
			System.out.print(a + " ");
		}
		System.out.println();
		soot.Main.main(args);
	}

}
