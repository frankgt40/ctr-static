package edu.uci.ccai6;

import java.util.LinkedList;

import edu.osu.cse.pa.spg.AbstractAllocNode;
import soot.jimple.toolkits.callgraph.Edge;

public class VisitedPair {
	AbstractAllocNode o;
	LinkedList<Edge> TM;
	public AbstractAllocNode getO() {
		return o;
	}
	public void setO(AbstractAllocNode o) {
		this.o = o;
	}
	public LinkedList<Edge> getTM() {
		return TM;
	}
	public void setTM(LinkedList<Edge> tM) {
		TM = tM;
	}
	public VisitedPair(AbstractAllocNode o, LinkedList<Edge> tM) {
		super();
		this.o = o;
		TM = tM;
	}
	
}
