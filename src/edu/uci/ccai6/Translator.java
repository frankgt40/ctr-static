package edu.uci.ccai6;

import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import alias.CallSite;
import alias.FldPair;
import alias.Pair;
import edu.osu.cse.pa.Main;
import edu.osu.cse.pa.spg.AbstractAllocNode;
import edu.osu.cse.pa.spg.AbstractSPGEdge;
import edu.osu.cse.pa.spg.EntryEdge;
import edu.osu.cse.pa.spg.ExitEdge;
import edu.osu.cse.pa.spg.FieldPTEdge;
import edu.osu.cse.pa.spg.NodeFactory;
import edu.osu.cse.pa.spg.VarNode;
import edu.osu.cse.pa.spg.symbolic.SymbolicObject;
import edu.uci.ccai6.dumps.Leak;
import edu.uci.ccai6.util.Helper;
import edu.uci.ccai6.util.XMLParser;
import java_cup.reduce_action;

import java.util.ArrayList;

import soot.Local;
import soot.PatchingChain;
import soot.Scene;
import soot.SootField;
import soot.SootMethod;
import soot.Unit;
import soot.ValueBox;
import soot.jimple.AssignStmt;
import soot.jimple.NewExpr;
import soot.jimple.Stmt;
import soot.jimple.internal.AbstractNewExpr;
import soot.jimple.internal.JNewArrayExpr;
import soot.jimple.internal.JNewExpr;
import soot.jimple.toolkits.callgraph.CallGraph;
import soot.jimple.toolkits.callgraph.Edge;
import soot.jimple.toolkits.callgraph.ReachableMethods;
import soot.tagkit.LineNumberTag;
import soot.tagkit.Tag;
import soot.util.Chain;

public class Translator {
	private static Translator translator;
	////
	public static CEPair pair = null;
	private static SootMethod mo = null, mv = null;
	private static Local lo = null, lv = null;
	private static Stmt lastUseStmt = null;
	private static Stmt allocStmt = null;
	private static OP op = OP.NULL;
	private static SiStack callingContext = new SiStack();

	////
	public static LinkedList<Tuple> workList = new LinkedList<Tuple>();
	public static int N_BUDGET = 1000;
	////
	public static edu.osu.cse.pa.Main maa;
	////
	public static final int MAX_ITERATION = 20000;
	private static final int N = 7;
	////
	List<Leak> leakList = new ArrayList<Leak>();

	////
	private Translator() {

	}

	public static String printPath(List<AbstractSPGEdge> ctxPath) {
		String rsl = "";
		for (AbstractSPGEdge e : ctxPath) {
			rsl += e.src().toString() + " --> " + e.tgt().toString() + ";";
		}
		return rsl;
	}
	private void clear() {
		pair = null;
		mo = null;
		mv = null;
		lo = null;
		lv = null;
		lastUseStmt = null;
		allocStmt = null;
		callingContext.clear();
//		siStackSet.clear();
		workList.clear();
//		ctxStk.clear();
	}

	public static Translator v() {
		if (translator == null)
			translator = new Translator();
		return translator;
	}

	public void buildLeakList(String fileName) {
		XMLParser parser = new XMLParser(fileName);
		this.leakList = parser.loadData();
	}
	
	public boolean isSubType(String type, String sub) {
		if (sub.contains("ArrayList") && type.contains("List"))
			return true;
		else if(sub.contains("HashMap")&& type.contains("Map"))
			return true;
		else if(sub.contains("FOInputHandler") && type.contains("InputHandler"))
			return true;
		else if(sub.contains("PropertyList") && type.contains("Map") || sub.contains("PropertyList") && type.contains("HashMap"))
			return true;
		else
			return false;
	}

	public void setData(Leak leak) {
		String type = leak.getType();
		String leakObjName = leak.getLeakObjName();
		String allocObjName = leak.getAllocObjName();

		// Get local from allocation site 
		Site allocSite = leak.getAllocSite();
		SootMethod allocMtd = allocSite.getMtd();
		Translator.mo = allocMtd;
		PatchingChain<Unit> units = allocMtd.retrieveActiveBody().getUnits();
		for (Unit u : units) {
			int lineNumber = Helper.getLineNumFromTag(u);
			if (lineNumber == allocSite.getLineNum()) {
				List<ValueBox> bs = u.getUseBoxes();
				for (ValueBox box : bs) {
					// Here we don't get the local by the exact name of the variable pointing the object, since we used Jimple: tmp0 = new A; a = tmp0;
					if (box.getValue() instanceof NewExpr || box.getValue() instanceof JNewExpr || box.getValue() instanceof JNewArrayExpr) {
						if (u.toString().contains(type.replaceAll("\\[\\]", ""))) {
							// Get allocation object and method
							List<ValueBox> listOfBox = u.getDefBoxes();
							for (ValueBox b : listOfBox) {
								Local local = (Local) b.getValue();
								Translator.lo = local;
								Translator.allocStmt = (Stmt) u;
								break; // Make sure there is only one that matches this local and statement
							}
						}
						break; // Make sure only one line are procedured which is matched with the linenumber
					}
				}
				
			}

		}
		Chain<Local> locals = allocMtd.getActiveBody().getLocals();
		if (allocObjName != null && Translator.lo == null) {
			for (Local local : locals) {
				if (local.getName().equals(allocObjName)) {
					Translator.lo = local;
					break;
				}
			}

			for (Unit u : units) {
				int lineNum = Helper.getLineNumFromTag(u);
				if (/* don't need to compare linenumber, lastUseStmt can be relaxed lineNum == lastUseLineNum && */ 
						 u.toString().contains(Translator.lo.getName()) ) {
					Translator.allocStmt = (Stmt) u;
					break;
				}
			}
		}

		// Get local from last-use site
		Site lastUseSite = leak.getLastUseSite();
		SootMethod lastUseMtd = lastUseSite.getMtd();
		int lastUseLineNum = lastUseSite.getLineNum();
		Translator.mv = lastUseMtd;
		units = lastUseMtd.getActiveBody().getUnits();

		locals = lastUseMtd.getActiveBody().getLocals();
		for (Local local : locals) {
			if (local.getName().equals(leakObjName)) {
				Translator.lv = local;
				break;
			}
		}
		
		if (lv == null) {
			for (Unit u : units) {
				int lineNumber = Helper.getLineNumFromTag(u);
				if (lineNumber == lastUseSite.getLineNum()) {
					List<ValueBox> bs = u.getDefBoxes();
					for (ValueBox box : bs) {
						List<ValueBox> listOfBox = u.getDefBoxes();
						for (ValueBox b : listOfBox) {
							Local local = (Local) b.getValue();
							String localType = local.getType().toString();
							if (localType.contains(type) || isSubType(localType, type)) {
								Translator.lv = local;
								Translator.lastUseStmt = (Stmt) u;
								break;
							}
							break; // Make sure there is only one that matches this local and statement
						}
						break; // Make sure only one line are procedured which is matched with the linenumber
					}
				}			
			}			
		} else {
			for (Unit u : units) {
				int lineNum = Helper.getLineNumFromTag(u);
				if (/* don't need to compare linenumber, lastUseStmt can be relaxed lineNum == lastUseLineNum && */ 
						 u.toString().contains(Translator.lv.getName()) ) {
					Translator.lastUseStmt = (Stmt) u;
					break;
				}
			}
		}
		

		Stmt stmt = null;
		int i = 1;
		try {
			// Process Calling context
			List<Site> ctx = leak.getCallingContext();
			for (; i < ctx.size(); i++) { // Starts from index 1, because the first line may not contain a call-site
				if (i==21)
					System.out.println();
				Site thisSite = ctx.get(i);
				SootMethod callee = ctx.get(i-1).getMtd();
				SootMethod caller = ctx.get(i).getMtd();
				stmt = thisSite.getStmt(callee.getName());
				CallGraph clGraph = Scene.v().getCallGraph();
				Edge e = clGraph.findEdge(stmt, callee); //new Edge(caller, stmt, callee);
				if (e != null)
					Translator.callingContext.add(e);
			}
		} catch(Exception e) {
			System.out.println(i + ". : " + stmt+"\n\t"+e.getMessage());
			
		}
		

		lastUseSite.printSite();// ///////
		System.out.println("Finished3! lo: " + Translator.lo.getName() + " mo: "
				+ Translator.mo.getSignature() + "; lv: " + Translator.lv.getName()
				+ ", mv: " + Translator.mv.getSignature());
		System.out.println("Alloc Stmt: " + allocStmt.toString());
		System.out.println("Last use stmt: " + lastUseStmt.toString());
		System.out.println("Calling context: ");
		for (Edge e : Translator.callingContext)
			System.out.println(e);;

	

		ReachableMethods methods = Scene.v().getReachableMethods();
		System.out.println("Contains mv: " + methods.contains(mv));
		System.out.println("Contains mo: " + methods.contains(mo));
		// Construct Event tuple
		SootField field = null;// (SootField)lastUseStmt.getFieldRef().getField();
		AbstractEvent e;
		if (leak.getOpType().equals("r"))
			e = new AbstractEvent(lo, mo, lv, mv, field, lastUseStmt, OP.READ);
		else 
			e = new AbstractEvent(lo, mo, lv, mv, field, lastUseStmt, OP.WRITE);
		System.err.println("AbstractEvent: " + e.toString());

		// Construct CE Pair
		pair = new CEPair(callingContext, e);
		System.err.println("CEPair: " + pair.toString());
	}


	public Site getLastUseSite(Leak leak) {
		Site lastSite = leak.getCallingContext().remove(0);
		return lastSite;
	}
	public static void printSiStackSet(Set<SiStack> siStackSet) {
		System.out.print("Printing the SiStack: ");
		String prompt = "";
		for (SiStack siStack : siStackSet) {
			prompt += "One stack: \n" + printSiStack(siStack) + "\n";
		}
		System.out.println(prompt);
	}
	public static String printSiStack(SiStack siStack) {
		return siStack.toString();
	}
	public static LinkedList<ReferenceNode> reduceSH(LinkedList<ReferenceNode> sh) {
		LinkedList<ReferenceNode> rsl = new LinkedList<ReferenceNode>();
		
		for (ReferenceNode n1 : sh) {
			boolean haveIt = false;
			for (ReferenceNode n2 : rsl) {
				if (n2.equals(n1))
					haveIt = true;
			}
			if (!haveIt) rsl.add(n1);
		}
		return rsl;
	}
	
	// Only accept EntryEdge or ExitEdge
	private static AbstractSPGEdge reverseEdge(AbstractSPGEdge e) {
		AbstractSPGEdge rslE = null;

		AbstractAllocNode srcNode = e.src();
		AbstractAllocNode tgtNode = e.tgt();
		
		if (e instanceof EntryEdge) {
			Edge edge = ((EntryEdge) e).getCallGraphEdge();
			rslE = new ExitEdge(tgtNode, srcNode, edge);
		} else if (e instanceof ExitEdge) {
			Edge edge = ((ExitEdge) e).getCallGraphEdge();
			rslE = new EntryEdge(tgtNode, srcNode, edge);
		} else {
			System.err.println("Wrong type of Edge in reverseEdge function!");
			rslE = null;
		}
		
		return rslE;
	}
	public static SiStack getSiStack(LinkedList<Edge> ctxStack) {
		SiStack siStack = new SiStack(ctxStack);
		return siStack;
	}
	private void translateOneLeak(Leak leak) {
		setData(leak);

		long start = System.currentTimeMillis();
		HashSet<LinkedList<ReferenceNode>> refPaths = maa.CR(pair); // Core
		long end = System.currentTimeMillis();
		long delta = end - start;
		
		refPaths = removeDuplications(refPaths);
		System.out.println("Number of reference paths: " + (refPaths.size()));
		
		AECUtil.out.println("ID: " + leak.getNo());
		AECUtil.out.println("Number of reference paths: " + (refPaths.size()));
		for (LinkedList<ReferenceNode> refPath : refPaths) {
			printRefPath(refPath);
		}
		AECUtil.out.println("Time: " + delta + " ms");
		AECUtil.out.println();
		
		System.out.println("Time: " + delta + " ms");
	}
	public boolean isSubPath(LinkedList<ReferenceNode> path, LinkedList<ReferenceNode> subPath) {
		if (path == null || subPath == null) return false;
		if (subPath.size() > path.size()) return false;
		
		int i = path.size()-1, j = subPath.size()-1;
		for (; j >= 0; j--,j--) {
			ReferenceNode pathNode = path.get(i);
			ReferenceNode subPathNode = subPath.get(j);
			if (!pathNode.from.equals(subPathNode.from) || !pathNode.ptFld.equals(subPathNode.ptFld))
				return false;
		}
		return true;
	}
	public HashSet<LinkedList<ReferenceNode>> removeDuplications(HashSet<LinkedList<ReferenceNode>> refPaths) {
		LinkedList<LinkedList<ReferenceNode>> paths = new LinkedList<LinkedList<ReferenceNode>>(refPaths);
		for (int i = 0; i < paths.size(); i++) {
			for (int j = 0; j < paths.size(); j++) {
				if (j == i) continue;
				
				// Check whether paths[i] is sub-path of paths[j]
				if (isSubPath(paths.get(j),paths.get(i))) {
					paths.set(i, null);
					break;
				}
			}
		}
		for (int i = 0; i < paths.size(); i++) {
			if (paths.get(i) == null) paths.remove(i);
		}
		HashSet<LinkedList<ReferenceNode>> rsl = new HashSet<LinkedList<ReferenceNode>>(paths);
		return rsl;
	}
	public void printRefPath(LinkedList<ReferenceNode> refPath){
		if(refPath == null) return;
		System.out.println("###One reference path:");
		AECUtil.out.println("###One reference path:");
		for (ReferenceNode n : refPath) {
			System.out.print(n.from.toString()+"--"+n.ptFld.getName()+"-->");
			AECUtil.out.print(n.from.toString()+"--"+n.ptFld.getName()+"-->");
		}
		System.out.println(edu.osu.cse.pa.Main.allocNode);
		AECUtil.out.println(edu.osu.cse.pa.Main.allocNode);
	}

	public static void printWorkList(LinkedList<Tuple> workList) {
		for (Tuple tuple : workList) {
			tuple.print();
		}
	}
	
	public static Set<Pair<AbstractAllocNode, SiStack>> getRealPairs(Set<Pair<AbstractAllocNode, SiStack>> unRealPairs) {
		Set<Pair<AbstractAllocNode, SiStack>> pairs = new HashSet<Pair<AbstractAllocNode, SiStack>>(); // The one don not contain duplicated pairs
		
		for (Pair<AbstractAllocNode, SiStack> pair : unRealPairs) {
			if (pairs.isEmpty()) {
				pairs.add(pair);
			} else {
				// check if we already have it?
				boolean haveit = false;
				for (Pair<AbstractAllocNode, SiStack> weHave : pairs) {
					if (pair.first.equals(weHave.first)) 
						haveit = true;
				}
				if (!haveit) pairs.add(pair);
			}
		}
		
		return pairs;
	}
		
	public void translate() {

		if (leakList == null || leakList.isEmpty()) {
			System.err.println("Error! leakList has not been built.");
			System.exit(-1);
		}

		for (Leak leak : leakList) {
			clear(); // Clear last leak data
			translateOneLeak(leak);
//			break;
		}
	}
}
