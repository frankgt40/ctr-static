package edu.uci.ccai6;

import edu.osu.cse.pa.spg.*;
import soot.jimple.toolkits.callgraph.Edge;

import java.util.*;


@SuppressWarnings("serial")
public class RPair {
	AbstractAllocNode o;
	LinkedList<Edge> cxtStk; // all parenthesis should be opening
	public AbstractAllocNode getO() {
		return o;
	}
	public void setO(AbstractAllocNode o) {
		this.o = o;
	}
	public LinkedList<Edge> getCxtStk() {
		return cxtStk;
	}
	public void setCxtStk(LinkedList<Edge> cxtStk) {
		this.cxtStk = cxtStk;
	}
	public RPair(AbstractAllocNode o, LinkedList<Edge> cxtStk) {
		super();
		this.o = o;
		this.cxtStk = cxtStk;
	}
	
	
}
