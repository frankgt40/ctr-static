package edu.uci.ccai6.util;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import edu.uci.ccai6.dumps.HeapDump;
import edu.uci.ccai6.dumps.HeapObj;

public class DumpReader {
	XMLParser parser;
	ReferenceTree tree = new ReferenceTree();
	public DumpReader(String xmlFileName) {
		parser = new XMLParser(xmlFileName);
	}
	public void computeRefPaths() {
		LinkedList<HeapDump> heapDumps = parser.loadHeapDumps();
		
		
		if (heapDumps != null) {
			for (HeapDump dump : heapDumps) { 
				tree.add(dump);
			}
		}
		
		tree.print();
			
	}
	public static void main(String[] args) {
		DumpReader reader = new DumpReader("./heapDumps/luIndexDumps.txt");
		reader.computeRefPaths();
	}
}
