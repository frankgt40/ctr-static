package edu.uci.ccai6.util;

import java.io.File;
import java.util.LinkedList;
import java.util.List;
import java.util.ArrayList;

import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.DocumentBuilder;

import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.w3c.dom.Node;
import org.w3c.dom.Element;

import edu.uci.ccai6.dumps.AllocSite;
import edu.uci.ccai6.dumps.HeapDump;
import edu.uci.ccai6.dumps.HeapObj;
import edu.uci.ccai6.dumps.Leak;

public class XMLParser {
	File inputFile;
	
	public XMLParser(String fileName) {
		inputFile = new File(fileName);
	}

	public List<Leak> loadData2() {
		List<Leak> rsl = null;
		try {
			rsl = new ArrayList<Leak>();
			DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
			Document doc = dBuilder.parse(inputFile);
			doc.getDocumentElement().normalize();
			System.out.println("Root element :" + doc.getDocumentElement().getNodeName());
			NodeList nList = doc.getElementsByTagName("Leaky");
			System.out.println("----------------------------");
			for (int temp = 0; temp < nList.getLength(); temp++) {
				Node nNode = nList.item(temp);
				System.out.println("\nCurrent Element :" + nNode.getNodeName());
				if (nNode.getNodeType() == Node.ELEMENT_NODE) {
					Element eElement = (Element) nNode;
					System.out.println("No: " + eElement.getAttribute("No"));
					String callingContext = eElement.getElementsByTagName("CallingContext").item(0).getTextContent();
					System.out.println("CallingContext : " + callingContext);
					Node subNode = eElement.getElementsByTagName("LeakyObj").item(0);
					Element subElement = (Element) subNode;
					
					String allocSite = subElement.getElementsByTagName("AllocSite").item(0).getTextContent();
					System.out.println("AllocSite: " + allocSite);
					String type  = subElement.getElementsByTagName("type").item(0).getTextContent();
					Leak leak = new Leak(callingContext, allocSite, type);
					if (leak != null)
						rsl.add(leak);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return rsl;
	}
	public List<Leak> loadData() {
		List<Leak> rsl = null;
		try {
			rsl = new ArrayList<Leak>();
			DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
			Document doc = dBuilder.parse(inputFile);
			doc.getDocumentElement().normalize();
			System.out.println("Root element :" + doc.getDocumentElement().getNodeName());
			NodeList nList = doc.getElementsByTagName("Leaky");
			System.out.println("----------------------------");
			for (int temp = 0; temp < nList.getLength(); temp++) {
				Node nNode = nList.item(temp);
				System.out.println("\nCurrent Element :" + nNode.getNodeName());
				if (nNode.getNodeType() == Node.ELEMENT_NODE) {
					Element eElement = (Element) nNode;
					System.out.println("No: " + eElement.getAttribute("No"));
					String no = eElement.getAttribute("No");
					String lastUseSite = eElement.getElementsByTagName("LastUseSite").item(0).getTextContent();
					System.out.println("LastUseSite : " + lastUseSite);
					String allocSite = eElement.getElementsByTagName("AllocSite").item(0).getTextContent();
					String lastUseName = eElement.getElementsByTagName("LastUseName").item(0).getTextContent();
					String allocName = eElement.getElementsByTagName("AllocName").item(0).getTextContent();
					String type = eElement.getElementsByTagName("Type").item(0).getTextContent();
					String opType = eElement.getElementsByTagName("OPTYPE").item(0).getTextContent();
					
					Leak leak = new Leak(no, lastUseSite, allocSite, type, lastUseName, allocName, opType);
					if (leak != null)
						rsl.add(leak);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return rsl;
	}

	public LinkedList<HeapDump> loadHeapDumps() {
		LinkedList<HeapDump> heapDumps = null;
		try {
			heapDumps = new LinkedList<HeapDump>();
			DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
			Document doc = dBuilder.parse(inputFile);
			doc.getDocumentElement().normalize();
			System.out.println("Root element :" + doc.getDocumentElement().getNodeName());
			NodeList nList = doc.getElementsByTagName("HeapDump");
			System.out.println("----------------------------");
			for (int temp = 0; temp < nList.getLength(); temp++) {
				Node nNode = nList.item(temp);
				//System.out.println("\nCurrent Element :" + nNode.getNodeName());
				if (nNode.getNodeType() == Node.ELEMENT_NODE) {
					Element eElement = (Element) nNode;
					int no =  Integer.parseInt(eElement.getAttribute("No"));
					
					//----------------From object--------------------//
					Element fromObjElement = (Element) eElement.getElementsByTagName("From").item(0);
					String type = fromObjElement.getElementsByTagName("Type").item(0).getTextContent();
					String allocSite = fromObjElement.getElementsByTagName("AllocSite").item(0).getTextContent();
					String address = fromObjElement.getElementsByTagName("Addr").item(0).getTextContent();
					HeapObj from = new HeapObj(type, new AllocSite(allocSite), Long.parseLong(address));
					
					//----------------To object--------------------//
					Element toObjElement = (Element) eElement.getElementsByTagName("To").item(0);
					NodeList list = null;
					list = toObjElement.getElementsByTagName("Type");
					if (list != null)
						if (list.item(0) != null)
							type = list.item(0).getTextContent();
						else
							type = "";
					list = toObjElement.getElementsByTagName("AllocSite");
					if (list != null)
						if (list.item(0) != null)
							allocSite = list.item(0).getTextContent();
						else
							allocSite = "";
					list = toObjElement.getElementsByTagName("Addr");
					if (list != null)
						if (list.item(0) != null)
							address = list.item(0).getTextContent();
						else
							address = "";
					HeapObj to = new HeapObj(type, new AllocSite(allocSite), Long.parseLong(address));
					
					heapDumps.add(new HeapDump(no, from, to));
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return heapDumps;
	}
	public static void main(String[] args) {

	}
}
