package edu.uci.ccai6.util;

import java.util.LinkedList;

import edu.uci.ccai6.dumps.HeapDump;
import edu.uci.ccai6.dumps.HeapObj;

public class ReferenceTree {
	private LinkedList<ReferenceTreeNode> roots = new LinkedList<ReferenceTreeNode>();
	public void add(HeapDump heapDump) {
		if (!heapDump.to.hasAllocSite()) return;
		
		ReferenceTreeNode target = findNode(heapDump.from);
		if (target == null) {
			ReferenceTreeNode anotherRoot = new ReferenceTreeNode(heapDump);
			roots.add(anotherRoot);
		} else {
			target.addKid(heapDump.to);
		}
	}
	// If cannot find the node return 'null'
	public ReferenceTreeNode findNode(HeapObj obj) {
		ReferenceTreeNode rsl = null;
		for (ReferenceTreeNode root: roots) {
			rsl = root.findChild(obj);
			if (rsl != null) return rsl;
		}
		return rsl;
	}
	public void print() {
		for (ReferenceTreeNode root : roots) {
			System.out.println("A tree: ");
			root.print(0);
		}
	}
}
