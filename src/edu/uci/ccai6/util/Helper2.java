package edu.uci.ccai6.util;

import java.util.Iterator;

import edu.uci.ccai6.Site;
import soot.PatchingChain;
import soot.Scene;
import soot.SootClass;
import soot.SootMethod;
import soot.Unit;
import soot.jimple.Stmt;
import soot.tagkit.LineNumberTag;
import soot.tagkit.Tag;

public class Helper2 {
	public static String realType(String type) {
		if (type.equals("Z"))
			return "boolean";
		else if (type.equals("V"))
			return "void";
		else if (type.equals("I")) {
			return "int";
		} else if (type.equals("C")) {
			return "char";
		} else if (type.equals("B")) {
			return "byte";
		} else if (type.isEmpty() || type.trim().equals("")){
			return "void";
		} else {
			type = type.replace("\n","").replace("\t", "");
			if (type.startsWith("L")) {
				type = type.substring(1).replace('/', '.');
			}
			type = type.replace(";", "").trim();
			return type;
		}
	}

	public static Site lineToSite(String line) {
		String[] strList = line.split("\\.");
		if (strList.length != 2) {
			System.err.println("Error! strList length is not 2! It's " + strList.length);
			System.err.println("line:" + line);
			System.exit(-1);
		}
		// Ldacapo/TestHarness;.runBenchmark (Ljava/io/File;Ljava/lang/String;Ldacapo/TestHarness;)V:302 (bci = 356)
		String type = strList[0];
		String suffix = strList[1];
		type = type.replace("\n","").replace("\t", "");
		suffix = suffix.replace("\n","").replace("\t", "");
		if (type.startsWith("L")) {
			type = type.substring(1).replace('/', '.').replace(";", "").trim();
		}
		String method = suffix.substring(0, suffix.indexOf(')') + 1).replace(';', ',').replace('/', '.');
		String pair = suffix.substring(suffix.indexOf(')')+1, suffix.length()).split(" ")[0];

		String returnType = pair.split(":")[0];
		returnType = realType(returnType);
		if (returnType.startsWith("L")) {
			returnType = returnType.substring(1).replace('/', '.').replace(";", "").trim();
		}
		
		int lineNum = -1;
		lineNum = Integer.parseInt(pair.split(":")[1]);
		
//		System.out.println("returnType: " + returnType + ", lineNum: " + lineNum);
		
		String methodName = method.split(" ")[0];
		String methodArgs = method.split(" ")[1];
		methodArgs = methodArgs.replace("(", "");
		methodArgs = methodArgs.replace(")", "");
		String[] argStrList = null;
		if (!methodArgs.trim().isEmpty()) {
			argStrList = methodArgs.trim().split(",");
			methodArgs = "";
			for (int i = 0; i < argStrList.length; i++) {
				String tmp = argStrList[i].trim();
				if (tmp.startsWith("L")) // Normal type
					tmp = tmp.substring(1);
				else if (tmp.startsWith("[L")) { // Array type
					tmp = tmp.substring(2)+"[]";
				}
				tmp = tmp.trim();
				tmp = realType(tmp);
				methodArgs += tmp + ",";
				// System.out.println("methodArgs: " + methodArgs);
			}
			if (methodArgs.length() > 1)
				methodArgs = methodArgs.substring(0, methodArgs.length() - 1);
		}else {
			methodArgs = "";
		}
		String sig = methodName + "(" + methodArgs + ")";

		
		SootClass theCl = null;
		SootMethod theMtd = null;
		
//		Scene.v().loadClassAndSupport(type); // For some classes that are not loaded
		
		for (Iterator ci = Scene.v().getClasses().iterator(); ci.hasNext();) {
			SootClass cl = (SootClass) ci.next();
			if (cl.getName().equals(type)) {
				// System.out.println("Class: " + cl.getName());
				theCl = cl;
				for (Iterator mi = cl.getMethods().iterator(); mi.hasNext();) {
					SootMethod smt = (SootMethod) mi.next();
					if (smt.getSignature().contains(methodName)) { // May change to sig
//						System.out.println("Method: " + smt.getSignature());
						theMtd = smt;
						break;
					}
				}
			}
			if (theMtd != null) break;
		}
	
		
		if (theMtd != null && lineNum != -1) {
			return new Site(theMtd, lineNum);
		} else 
			return null;
	}

}
