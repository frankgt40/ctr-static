package edu.uci.ccai6.util;

import java.util.LinkedList;

import edu.uci.ccai6.dumps.HeapDump;
import edu.uci.ccai6.dumps.HeapObj;

public class ReferenceTreeNode {
	private HeapObj node;
	private LinkedList<ReferenceTreeNode> children = new LinkedList<ReferenceTreeNode>();
	public ReferenceTreeNode(HeapDump heapDump) {
		node = heapDump.from;
		addKid(heapDump.to);
	}
	public ReferenceTreeNode(HeapObj obj) {
		this.node = obj;
	}
	public HeapObj getNode() {
		return node;
	}
	public boolean equals(HeapObj obj) {
		if (obj == null) return false;
		return obj.equals(node);
	}
	public void addKid(HeapObj obj) {
		for (ReferenceTreeNode child : children) {
			if (child.getNode().equals(obj)) return;
		}
		
		ReferenceTreeNode kid = new ReferenceTreeNode(obj);
		children.add(kid);
	}
	public ReferenceTreeNode findChild(HeapObj obj) {
		ReferenceTreeNode rsl = null;
		if (node.equals(obj)) return this;
		else if (children.isEmpty()) return null;
		else {
			for (ReferenceTreeNode child : children) {
				rsl = child.findChild(obj);
				if (rsl != null) return rsl;
			}
		}
		return null;
	}
	public void print(int level) {
		for (int i = 0; i != level; i++) System.out.print("\t");
		
		System.out.println(node.toString());
		if (children.isEmpty()) return;
		for (ReferenceTreeNode child : children) {
			child.print(level+1);
		}
	}
}
