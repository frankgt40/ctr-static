package edu.uci.ccai6.util;

import java.util.Iterator;

import edu.uci.ccai6.Site;
import soot.PatchingChain;
import soot.Scene;
import soot.SootClass;
import soot.SootMethod;
import soot.Unit;
import soot.jimple.Stmt;
import soot.tagkit.LineNumberTag;
import soot.tagkit.Tag;
import soot.util.Chain;

public class Helper {
	public static String realType(String type) {
		if (type.equals("Z"))
			return "boolean";
		else if (type.equals("V"))
			return "void";
		else if (type.equals("I")) {
			return "int";
		} else if (type.equals("C")) {
			return "char";
		} else if (type.equals("B")) {
			return "byte";
		} else if (type.isEmpty() || type.trim().equals("")){
			return "void";
		} else {
			type = type.replace("\n","").replace("\t", "");
			if (type.startsWith("L")) {
				type = type.substring(1).replace('/', '.');
			}
			type = type.replace(";", "").trim();
			return type;
		}
	}
	public static int getLineNumFromTag(Unit u) {
		Integer l = new Integer(0);
		int iii = -1;
		try {
			byte[] v = u.getTag("SourceLnPosTag").getValue();
			iii = ((v[0] & 0xff)<<8 |(v[1] &0xff));
			return iii;
		} catch (Exception e) {
//			System.err.println("ERROR: " + u.toString());
			// Do nothing, either it will have SourceLnPosTag or LineNumberTag
		}
		for (Iterator i =  u.getTags().iterator(); i.hasNext();) {
			Tag tag = (Tag) i.next();
			if (tag instanceof LineNumberTag) {
				byte[] value = tag.getValue();
				int lineNumber = ((value[0] & 0xff) << 8) | (value[1] & 0xff);
				if (lineNumber != l.intValue())
					l = new Integer(lineNumber);
				return l.intValue();
			}
		}
//		System.err.println("Error! cannot get the line number from Unit: " + u.toString());
//		System.exit(-1);
		return -1;
//		return iii;
	}
	public static String toFullTypeName(String jvmType) {
		if (jvmType.equals("B")) {
			return "byte";
		} else if (jvmType.equals("C")) {
			return "char";
		} else if (jvmType.equals("D")) {
			return "double";
		} else if (jvmType.equals("F")) {
			return "float";
		} else if (jvmType.equals("I")) {
			return "int";
		} else if (jvmType.equals("J")) {
			return "long";
		} else if (jvmType.equals("S")) {
			return "short";
		} else if (jvmType.equals("Z")) {
			return "boolean";
		} else if (jvmType.equals("V")) {
			return "void";
		} else {
			System.err.println("Wrong type! Find me in Helper.java");
			System.exit(-1);
			return null;
		}
	}
	public static String convertTypeSig(String jvmType) { 
		String type = jvmType.trim();
		int arrayDimension = 0;
		// Get array dimension, e.g. [[Z --> boolean[][]
		while (type.startsWith("[")) {
			arrayDimension++;
			type = type.substring(1);
		}
		if (type.startsWith("L")) {
			// reference classname
			type = type.substring(1);
			type = type.replaceAll("/", ".");
			type = type.replaceAll(";", "");
		} else {
			type = toFullTypeName(type);
		}
		for (int i = 0; i < arrayDimension; i++) {
			type = type+"[]";
		}
		return type;
	}
	public static String convertParameterList(String parameterListJvm) {
		String rsl = "";
		if (parameterListJvm.isEmpty()) return rsl;
		parameterListJvm = parameterListJvm.trim();
		int start = 0;
		for (int end = 0; end < parameterListJvm.length(); end++) {
			if (parameterListJvm.charAt(end) == '[') {
				continue;
			} else if (parameterListJvm.charAt(end) == 'L') {
				end = parameterListJvm.indexOf(';', end);
				String type = parameterListJvm.substring(start, end);
				type = convertTypeSig(type);
				rsl = rsl+","+type;
				start = end+1;
			} else {
				String type = "";
				if (start ==  end) type = type+parameterListJvm.charAt(end);
				else type = parameterListJvm.substring(start, end+1);
				type = convertTypeSig(type);
				rsl = rsl+","+type;
				start = end+1;
			}
		}
		rsl = rsl.substring(1); // get rid of the first comma (i.e. ',')
		return rsl;
	}
	public static Site lineToSite(String line) {
		line = line.replaceAll(" \\(bci = .*\\)", "").trim();//(bci = 6)
		line = line.replaceAll(" \\( bci = .*\\)", "").trim();//( bci = 6)
		String[] strList = line.split("\\.");
		if (strList.length != 2) {
			System.err.println("Error! strList length is not 2! It's " + strList.length);
			System.err.println("line:" + line);
			System.exit(-1);
		}
		String className = strList[0].substring(0, strList[0].length()-1); // get rid of ";"
		className = convertTypeSig(className);
		
		String mtdSigJvm = strList[1];
		int openB = mtdSigJvm.indexOf('(');
		int closeB = mtdSigJvm.indexOf(')');
		String mtdName = mtdSigJvm.substring(0, openB);
		String parameterListJvm = mtdSigJvm.substring(openB+1, closeB);
		String parameterList = convertParameterList(parameterListJvm);
		
		String returnTypeJvm = mtdSigJvm.substring(closeB+1).split(":")[0];
		String returnType = convertTypeSig(returnTypeJvm);
		int lineNum = Integer.parseInt( mtdSigJvm.substring(closeB+1).split(":")[1] );
		String mtdSig = /*className.trim()+": "+*/returnType.trim()+" "+mtdName.trim().replace('[', '<').replace(']', '>')+"("+parameterList+")";
		String[] tmp = className.split("\\.");
		String clsNameNoSig = tmp[tmp.length-1];
		Chain<SootClass> klasses = Scene.v().getClasses();
//		for (SootClass k : klasses) {
//			System.out.println("Cls: " + k.getName());
//		}
//		System.out.println("Number of classes: " + klasses.size());
		SootClass klass = Scene.v().getSootClass(className); ////////////////////
		SootMethod mtd = klass.getMethod(mtdSig);
		Site site = new Site(mtd, lineNum);
		return site;
		
	}
	public static Site lineToSite2(String line) {
		String[] strList = line.split("\\.");
		if (strList.length != 2) {
			System.err.println("Error! strList length is not 2! It's " + strList.length);
			System.err.println("line:" + line);
			System.exit(-1);
		}
		// Ldacapo/TestHarness;.runBenchmark (Ljava/io/File;Ljava/lang/String;Ldacapo/TestHarness;)V:302 (bci = 356)
		String type = strList[0];
		String suffix = strList[1];
		type = type.replace("\n","").replace("\t", "");
		suffix = suffix.replace("\n","").replace("\t", "");
		if (type.startsWith("L")) {
			type = type.substring(1).replace('/', '.').replace(";", "").trim();
		}
		String method = suffix.substring(0, suffix.indexOf(')') + 1).replace(';', ',').replace('/', '.');
		String pair = suffix.substring(suffix.indexOf(')')+1, suffix.length()).split(" ")[0];

		String returnType = pair.split(":")[0];
		returnType = realType(returnType);
		if (returnType.startsWith("L")) {
			returnType = returnType.substring(1).replace('/', '.').replace(";", "").trim();
		}
		
		int lineNum = -1;
		lineNum = Integer.parseInt(pair.split(":")[1]);
		
//		System.out.println("returnType: " + returnType + ", lineNum: " + lineNum);
		
		String methodName = method.split(" ")[0];
		String methodArgs = method.split(" ")[1];
		methodArgs = methodArgs.replace("(", "");
		methodArgs = methodArgs.replace(")", "");
		String[] argStrList = null;
		if (!methodArgs.trim().isEmpty()) {
			argStrList = methodArgs.trim().split(",");
			methodArgs = "";
			for (int i = 0; i < argStrList.length; i++) {
				String tmp = argStrList[i].trim();
				if (tmp.startsWith("L")) // Normal type
					tmp = tmp.substring(1);
				else if (tmp.startsWith("[L")) { // Array type
					tmp = tmp.substring(2)+"[]";
				}
				tmp = tmp.trim();
				tmp = realType(tmp);
				methodArgs += tmp + ",";
				// System.out.println("methodArgs: " + methodArgs);
			}
			if (methodArgs.length() > 1)
				methodArgs = methodArgs.substring(0, methodArgs.length() - 1);
		}else {
			methodArgs = "";
		}
		String sig = methodName + "(" + methodArgs + ")";

		
		SootClass theCl = null;
		SootMethod theMtd = null;
		
//		Scene.v().loadClassAndSupport(type); // For some classes that are not loaded
		
		for (Iterator ci = Scene.v().getClasses().iterator(); ci.hasNext();) {
			SootClass cl = (SootClass) ci.next();
			if (cl.getName().equals(type)) {
				// System.out.println("Class: " + cl.getName());
				theCl = cl;
				String jjj = cl.getJavaStyleName();
				for (Iterator mi = cl.getMethods().iterator(); mi.hasNext();) {
					SootMethod smt = (SootMethod) mi.next();
					String sig2 = smt.getSignature();
					if (smt.getSignature().contains(methodName)) { // May change to sig
//						System.out.println("Method: " + smt.getSignature());
						theMtd = smt;
						break;
					}
				}
				SootMethod sm2 = Scene.v().getMethod(line.trim());
			}
			if (theMtd != null) break;
		}
	
		
		if (theMtd != null && lineNum != -1) {
			return new Site(theMtd, lineNum);
		} else 
			return null;
	}
	public static void main(String[] args){
		String lineJvm = "LTest;.nonesense (ILjava/lang/String;Z[LA;[[C)I:19 (bci = 44)";
		lineToSite(lineJvm);
	}
}
