package edu.uci.ccai6.util;

import java.io.PrintStream;
import java.util.ArrayList;
import java.util.List;

import edu.osu.cse.pa.spg.AbstractSPGEdge;


public class Drawer {
	private PrintStream out;
	private static Drawer drawer;
	private List list;
	private Drawer() {
		
	}
	public void addObj(Object obj) {
		list.add(obj);
	}
	public void pringList() {
		out.println("We are printing!:");
		for (int i =  0; i < list.size(); i++) {
			if (list.get(i) instanceof AbstractSPGEdge) {
				out.println("\t|");
				out.println("\t| " + list.get(i));
				out.println("\tV");
			} else {
				out.println(list.get(i));
			}
		}
	}
	public static Drawer v() {
		if (drawer == null) {
			drawer = new Drawer();
			drawer.list = new ArrayList();
			if (drawer.out == null)
				drawer.out = System.err;
		}
		return drawer;
	}
	public void setDrawer(PrintStream out) {
		out = out;
	}
	public void draw(Object obj) {
		out.println(obj.toString());
	}
}
