package edu.uci.ccai6.util;

import java.util.HashSet;
import java.util.LinkedList;

import edu.uci.ccai6.dumps.HeapObj;

public class RefPaths{
	public LinkedList<HeapObj> path = new LinkedList<HeapObj>();

	public LinkedList<HeapObj> getPath() {
		return path;
	}

	public void setPath(LinkedList<HeapObj> path) {
		this.path = path;
	}
	
	
}
