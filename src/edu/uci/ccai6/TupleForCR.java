package edu.uci.ccai6;

import java.util.LinkedList;

import edu.osu.cse.pa.spg.AbstractAllocNode;
import soot.jimple.toolkits.callgraph.Edge;

public class TupleForCR {
	private AbstractAllocNode o;
	LinkedList<Edge> cxtStk; // all parenthesis should be all opening 
	LinkedList<ReferenceNode> TH;
	boolean b;
	public AbstractAllocNode getO() {
		return o;
	}
	public void setO(AbstractAllocNode o) {
		this.o = o;
	}
	public LinkedList<Edge> getCxtStk() {
		return cxtStk;
	}
	public void setCxtStk(LinkedList<Edge> cxtStk) {
		this.cxtStk = cxtStk;
	}
	public LinkedList<ReferenceNode> getTH() {
		return TH;
	}
	public void setTH(LinkedList<ReferenceNode> tH) {
		TH = tH;
	}
	public boolean isB() {
		return b;
	}
	public void setB(boolean b) {
		this.b = b;
	}
	public TupleForCR(AbstractAllocNode o, LinkedList<Edge> cxtStk,
			LinkedList<ReferenceNode> tH, boolean b) {
		super();
		this.o = o;
		this.cxtStk = cxtStk;
		TH = tH;
		this.b = b;
	}
	public LinkedList<Edge> getTM() {
		return this.cxtStk;
	}
}
