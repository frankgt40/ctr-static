package edu.uci.ccai6;

import java.util.HashSet;
import java.util.LinkedList;

import alias.FldPair;
import alias.Pair;
import edu.osu.cse.pa.spg.AbstractAllocNode;
import edu.osu.cse.pa.spg.AbstractNode;
import edu.osu.cse.pa.spg.AbstractSPGEdge;
import edu.osu.cse.pa.spg.FieldVarNode;

public class Tuple {
	public AbstractAllocNode o;
	public SiStack ctxStk;
	public LinkedList<ReferenceNode> fields;
	public boolean b = false;
	private HashSet<AbstractSPGEdge> visitedCtxEdges;
	private HashSet<Pair<FldPair, Integer>> visitedFldEdges; // with context snapshot
	private int ctxHash;

	// public Tuple(AbstractAllocNode o, SiStack ctxStk,
	// LinkedList<ReferenceNode> fields, boolean b) {
	// this.o = o;
	// this.ctxStk = ctxStk;
	// this.fields = fields;
	// this.b = b;
	// }

	public void print() {
		System.out.println("Printing tuple:\n" + "[o: " + o.toString() + "]\n"
				+ "[SiStackSet: " + ctxStk.toString() + "]\n" + "[delta: "
				+ fields.toString() + "]\n" + "[b: " + b + "]");
	}

	public Tuple(AbstractAllocNode o, SiStack ctxStk,
			LinkedList<ReferenceNode> fields, boolean b,
			HashSet<AbstractSPGEdge> visitedCtxEdges,
			HashSet<Pair<FldPair, Integer>> visitedFldEdges, int ctxHash) {
		super();
		this.o = o;
		this.ctxStk = ctxStk;
		this.fields = fields;
		this.b = b;
		this.visitedCtxEdges = visitedCtxEdges;
		this.visitedFldEdges = visitedFldEdges;
		this.ctxHash = ctxHash;
	}
}
