package edu.uci.ccai6;


public class CEPair {
	public SiStack siStack = null;
	public AbstractEvent e = null;
	public int n = 10; // thershold for TH

	public CEPair(SiStack siStack, AbstractEvent e) {
		this.siStack = siStack;
		this.e = e;
	}
	public String toString() {
		String rsl = "";
		
		rsl = "(c: " + siStack.toString() + ", e: " + e.toString() + ")";
		return rsl;
	}
}
