package edu.uci.ccai6;

import java.util.LinkedList;
import java.util.List;

import soot.jimple.toolkits.callgraph.Edge;

public class SiStack extends LinkedList<Edge>{
	public SiStack() {
		super();
	}
	public SiStack(LinkedList<Edge> superElmt) {
		super(superElmt);
	}

	public String toString() {
		String rsl = "";
		rsl = "[";
		for(Edge e : this) {
			rsl += e.toString() + ";";
		}
		return rsl;
	}
	public boolean isConsistent(SiStack shorter) {
		// check whether another is a prefix of this SiStack
		if (this.size() < shorter.size()) return false;
		
		for (int i = 0; i < shorter.size(); i++) {
			if (!shorter.get(i).equals(this.get(i))) 
				return false;
		}
		return true;
	}
	
	public boolean isConsistent(List<Site>  another) {
		// check whether another is a prefix of this SiStack
		if (this.size() < another.size()) return false;
		
		System.err.println("Unfinished!");
		System.exit(-1);
		return true;
	}
	
}
