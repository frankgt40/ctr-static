package edu.uci.ccai6;


import edu.osu.cse.pa.spg.VarNode;
import soot.Local;
import soot.SootField;
import soot.SootMethod;
import soot.Unit;

public class AbstractEvent {
	//<o, v, f', stmt, op'>
	public Local o;
	public SootMethod mo;
	public Local v;
	public SootMethod mv;
	public SootField f;
	public Unit stmt;
	public OP op;
	
	public AbstractEvent(Local o, SootMethod mo, Local v, SootMethod mv, SootField f, Unit stmt, OP op) {
		super();
		this.o = o;
		this.mo = mo;
		this.v = v;
		this.mv = mv;
		this.f = f;
		this.stmt = stmt;
		this.op = op;
	}
	
	public AbstractEvent() {
		
	}
	
	public String toString() {
		String rsl = "";
		rsl = "<[o:" + o +"], [v:" + v + "], [f:" + f + "], [stmt:" + stmt + "], [op: " + op + "]>";
		return rsl;
	}
}
enum OP{
	READ("READ"), 
	WRITE("WRITE"), 
	NULL("NULL");
	
	private OP(String value) {
		this.value = value;
	}
	public String getValue() {
		return value;
	}
	private final String value;
}
