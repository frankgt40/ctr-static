package edu.uci.ccai6;

import soot.jimple.toolkits.callgraph.Edge;

// Edges that stored in TM
public class TMEdge {
	private Edge e;
	public enum Type {
		OPEN, CLOSE
	}
	private Type type;
	public Edge getE() {
		return e;
	}
	public void setE(Edge e) {
		this.e = e;
	}
	public Type getType() {
		return type;
	}
	public void setType(Type type) {
		this.type = type;
	}
	public TMEdge(Edge e, Type type) {
		super();
		this.e = e;
		this.type = type;
	}
	public TMEdge() {
		super();
		this.e = null;
		this.type = null;
	}
	public boolean isOpen() {
		return type == Type.OPEN;
	}
	public boolean isNone() {
		return e == null;
	}
	
	public boolean match(TMEdge e2) {
		if (e2 == null) return false;
		if (this.type != e2.getType() && this.e.equals(e2.getE()) ) {
			return true;
		}
		return false;
	}
	
}
