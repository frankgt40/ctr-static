package edu.uci.ccai6;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Enumeration;
import java.util.Properties;

import alias.Util;

public class AECUtil {
	public static String cliCmd;
	public static String benchName;
	public static String outFormat;
	public static PrintWriter out;
	static {
		benchName = System.getProperty("benchName");
		String propFile = "."+File.separator+"properties"+File.separator+benchName+".property";
		String outputFile = "."+File.separator+"output"+File.separator+benchName;
		Properties props = new Properties();
		FileInputStream in;
		try {
			in = new FileInputStream(propFile);
			props.load(in);
			cliCmd = props.getProperty("cliCmd");
			Main.N_BUDGET = Integer.parseInt(props.getProperty("nodeBudget"));
			Util.SPA_BUDGET_NODES = Main.N_BUDGET;
			Util.DEBUG = Boolean.parseBoolean(props.getProperty("debug"));
			outFormat = props.getProperty("outFormat");
			cliCmd += " -f " + outFormat; 
			out = new PrintWriter(new FileWriter(outputFile));
		} catch (FileNotFoundException e) {
			System.err.println("Cannot find the property file! Has to terminate the analysis.");
			e.printStackTrace();
			System.exit(-1);
		} catch (IOException e) {
			System.err.println("Cannot load the property file! Has to terminate the analysis.");
			e.printStackTrace();
			System.exit(-1);
		} catch (SecurityException e) {
			System.err.println("System does not allow us to set the property field due to the security reasons.");
			e.printStackTrace();
			System.exit(-1);
		} catch (IllegalArgumentException e) {
			System.err.println("We cannot find the property field!");
			e.printStackTrace();
			System.exit(-1);
		} 
	}
}
