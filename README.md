# README 

This is the repository for CTR, a translation framework that can precisely infer, from a calling context of a method that contains a use of an object, the heap reference paths leading to the object at the time the use occurs.  

CTR provides immediate benefit to all dynamic analyses and bug finding tools such as memory leaks detectors and data race detectors by providing the reference path information from the stack traces (i.e., calling contexts), which, in turns, can help developers quickly find the root causes and develop fixes.

A full technical report is available. Please contact me at (ccai6@uci.edu) if you are interested.
    

# How do I run the code? 

* We use Soot to infer the reference paths from the calling contexts. We provide some sample calling contexts for memory leaks, which are generated by [CCU](https://dl.acm.org/citation.cfm?id=2544173.2509510), in ./input/fromDynamic/*. To get the reference paths for these leaky objects, use: 

  java -cp ./lib/soot-2.5.0.jar edu.uci.ccai6.Main -process-dir ./input/fromStatic/${BM} -cp ${JRE_HOME}/lib/rt.jar:${JRE_HOME}/lib/jce.jar:${JRE_HOME}/lib/jsse.jar:./deps/${BM}-deps.jar:./input/fromStatic/${BM}  -main-class dacapo.${BM}.Main  -w dacapo.${BM}.Main -refLen 10 -budget 200000

where,

  - BM is the name of benchmark. It can be one of the following: antlr, bloat, chart, eclipse, fop, hsqldb, jython, lusearch, luindex, pmd, xalan
  - refLen: CTR's option, specifying the maximum length of the reference paths. The higher the refLen, the more precise the result. 
  - budget: CTR's option, specifying the maximum number of graph nodes the framework traverses. The lower the budget, the less expensive the translation.

# Who do I talk to?

* If you have any questions, please contact me at (ccai6@uci.edu)