#/bin/bash

#written by Eric Bodden, McGill University,
#use at own risk

### USER DEFINED VARIABLES ###

#java runtime to analyze with Soot
JRE=/usr/lib/jvm/java-6-oracle/jre

#location of Soot
SOOT=/home/frankgt40/workspace/LearnSoot/test/soot-2.5.0.jar

#custom command line options for Soot
CUSTOM_OPTIONS=

#temporary directory in which to extract the benchmarks as input for Soot
TEMP=/home/frankgt40/Documents/POPL/ctr-static/input/fromStatic/preprocessing/benchmark/tmp

OUTDIR=/home/frankgt40/Documents/POPL/ctr-static/input/fromStatic/preprocessing/benchmark
### END OF USER DEFINED VARIABLES ###


# for BM in \
#         antlr bloat chart eclipse fop hsqldb jython luindex lusearch pmd xalan; \
# do \
BM=xalan

#prepare benchmark
BM_DIR="${TEMP}/${BM}"
echo preparing ${BM}...
rm -fr ${BM_DIR}
mkdir -p ${BM_DIR}
cp ${BM}.jar ${BM_DIR}
OLDDIR=`pwd`
cd ${BM_DIR}
jar xf ${BM}.jar
cd ${OLDDIR}

#process benchmark
# echo processing ${BM}...

# java -Xmx512m -cp ${SOOT} \
# soot.Main \
# -process-dir ${BM_DIR} \
# -soot-class-path ${JRE}/lib/rt.jar:${JRE}/lib/jce.jar:${JRE}/lib/jsse.jar:${BM}-deps.jar:${BM_DIR} \
# -main-class dacapo.${BM}.Main -d ${OUTDIR} -f J \
# ${CUSTOM_OPTIONS}

echo finished with ${BM}.

done
